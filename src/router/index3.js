import { Loading, Connection, Edit, FolderOpened } from '@element-plus/icons'
import { createRouter } from '/nf-ui-core'
import home from '../views/home.vue'

const _meta = {
  // 维护meta
  meta_base: {
    path: '/meta-base', title: '基础控件', icon: Edit,
    component: () => import('../views/meta/base/01-text.vue')
  },
  meta_form: {
    path: '/meta-form', title: '表单控件', icon: Edit,
    component: () => import('../views/meta/form/02-form.vue')
  },
  meta_form_card: {
    path: '/meta-form-card', title: '分栏表单', icon: Edit,
    component: () => import('../views/meta/form/03-form-card.vue')
  },
  meta_find: {
    path: '/meta-find', title: '查询控件', icon: Edit,
    component: () => import('../views/meta/find/20-find.vue')
  },
  meta_grid: {
    path: '/meta-grid', title: '列表控件', icon: Edit,
    component: () => import('../views/meta/grid/10-grid.vue')
  },
  meta_button: {
    path: '/meta-button', title: '操作按钮', icon: Edit,
    component: () => import('../views/meta/button/30-button.vue')
  }
}

const _test = {
  // 基础测试
  test_input: {
    path: '/test-html', title: '原生HTML', icon: Edit,
    component: () => import('../views/test/base/01-input.vue')
  },
  test_eltext: {
    path: '/test-el-text', title: 'UI库', icon: Edit,
    component: () => import('../views/test/base/02-eltext.vue')
  },
  test_transmit: {
    path: '/test-transmit', title: '传声筒', icon: Edit,
    component: () => import('../views/test/base/03-transmit.vue')
  },
  test_model: {
    path: '/test-model', title: '直接设置model', icon: Edit,
    component: () => import('../views/test/base/04-model.vue')
  }
}

const _slot = {
  test_slot: {
    path: '/test-slot', title: '插槽测试', icon: Edit,
    component: () => import('../views/test/slot/01-slot.vue')
  }
}

const _base = {
  // base
  base_html: {
    path: '/base-html', title: '原生HTML', icon: Edit,
    component: () => import('../views/ui/base/c-01html.vue')
  },
  base_ui: {
    path: '/base-ui', title: 'UI库组件', icon: Edit,
    component: () => import('../views/ui/base/c-02UI.vue')
  },
  base_transmit: {
    path: '/base-transmit', title: '传声筒', icon: Edit,
    component: () => import('../views/ui/base/c-03transmit.vue')
  },
  base_item: {
    path: '/base-item', title: '基础组件', icon: Edit,
    component: () => import('../views/ui/base/c-06item.vue')
  }
}

const _nf = {
  // nf
  nf_form: {
    path: '/nf-form', title: '表单控件', icon: Edit,
    component: () => import('../views/ui/form/index.vue')
  },
  nf_find: {
    path: '/nf-find', title: '查询控件', icon: Edit,
    component: () => import('../views/ui/find/index.vue')
  },
  nf_grid: {
    path: '/nf-grid', title: '列表控件', icon: Edit,
    component: () => import('../views/ui/grid/index.vue')
  },
  nf_button: {
    path: '/nf-button', title: '操作按钮', icon: Connection,
    component: () => import('../views/ui/button/index.vue')
  },
  nf_crud: {
    path: '/nf-crud', title: '增删改查', icon: Connection,
    component: () => import('../views/ui/crud/index.vue')
  },
  nf_list: {
    path: '/nf-list-page', title: '增删改查（多）', icon: Connection,
    component: () => import('../views/ui/crud/listPage.vue')
  }
}

const _routes = {}

Object.assign(_routes, _meta, _test, _slot, _base, _nf)

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: '/nf-rollup-ui-controller',
  /**
   * 首页
   */
  home: home,
  /**
   * 一级菜单，设计包含哪些二级菜单
   */
  group: [
    {
      id: "1", title: '设置属性', icon:  FolderOpened,
      children: [
        'meta_base',
        'meta_form',
        'meta_form_card',
        'meta_find',
        'meta_grid',
        'meta_button'
      ]
    },
    {
      id: "13", title: '测试', icon: FolderOpened,
      children: [
        'test_input',
        'test_eltext',
        'test_transmit',
        'test_model',
        'test_slot'
      ]
    },
    {
      id: "10", title: '基础控件', icon: FolderOpened,
      children: [
        'base_html',
        'base_ui',
        'base_transmit',
        'base_item'
      ]
    },
    {
      id: "12", title: '复合控件', icon: FolderOpened,
      children: [
        'nf_form',
        'nf_find',
        'nf_grid',
        'nf_button',
        'nf_crud',
        'nf_list'
      ]
    }
  ],

  /**
   * 二级菜单，设置路径、标题、图标和加载的组件
   */
  routes: _routes
   
})