import { Document, FolderOpened } from '@element-plus/icons'
// import { createRouter } from '@naturefw/ui-core'
import { createRouter } from '/nf-ui-core'

import home from '../views/home.vue'

// () => import('../views/test/base/01-input.vue')
import myInput from '../views/test/base/01-input.vue'


export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: '/nf-rollup-ui-controller',
  /**
   * 首页
   */
  home: home,

  menus: [
    /* 分到其他项目
    {
      menuId: '1',
      title: '设置属性',
      naviId: '0',
      path: 'meta',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '111', title: '基础控件', path: 'base', icon: Document,
          component: () => import('../views/meta/base/01-text.vue')
        },
        {
          menuId: '112', title: '表单控件', path: 'form', icon: Document,
          component: () => import('../views/meta/form/02-form.vue')
        },
        {
          menuId: '113', title: '分栏表单', path: 'form-card', icon: Document,
          component: () => import('../views/meta/form/03-form-card.vue')
        },
        {
          menuId: '114', title: '查询控件', path: 'find', icon: Document,
          component: () => import('../views/meta/find/20-find.vue')
        },
        {
          menuId: '115', title: '列表控件', path: 'grid', icon: Document,
          component: () => import('../views/meta/grid/10-grid.vue')
        },
        {
          menuId: '116', title: '操作按钮', path: 'button', icon: Document,
          component: () => import('../views/meta/button/30-button.vue')
        }
      ]
    },
    */
    {
      menuId: '13',
      title: '测试',
      naviId: '0',
      path: 'test',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '1311', title: '原生HTML', path: 'html', icon: Document,
          component: myInput // () => import('../views/test/base/01-input.vue')
        },
        {
          menuId: '1312', title: 'UI库组件', path: 'ui', icon: Document,
          component: () => import('../views/test/base/02-eltext.vue')
        },
        {
          menuId: '1313', title: '传声筒', path: 'transmit', icon: Document,
          component: () => import('../views/test/base/03-transmit.vue')
        },
        {
          menuId: '1314', title: '直接设置model', path: 'model', icon: Document,
          component: () => import('../views/test/base/04-model.vue')
        },
        {
          menuId: '1315', title: '插槽测试', path: 'slot', icon: Document,
          component: () => import('../views/test/slot/01-slot.vue')
        },
        {
          menuId: '1316', title: '添加新路由', path: 'new-router', icon: Document,
          component: () => import('../views/test/router/add.vue')
        }
      ]
    },
    {
      menuId: '10',
      title: '基础控件',
      naviId: '0',
      path: 'base',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '1011', title: '原生HTML', path: 'html', icon: Document,
          component: () => import('../views/ui/base/c-01html.vue')
        },
        {
          menuId: '1012', title: 'UI库组件', path: 'ui', icon: Document,
          component: () => import('../views/ui/base/c-02UI.vue')
        },
        {
          menuId: '1013', title: '传声筒', path: 'transmit', icon: Document,
          component: () => import('../views/ui/base/c-03transmit.vue')
        },
        {
          menuId: '1014', title: '基础组件', path: 'item', icon: Document,
          component: () => import('../views/ui/base/c-06item.vue')
        }
      ]
    },
    {
      menuId: '12',
      title: '复合控件',
      naviId: '0',
      path: 'nf',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '1221', title: '表单控件', path: 'form', icon: Document,
          component: () => import('../views/ui/form/index.vue')
        },
        {
          menuId: '1222', title: '查询控件', path: 'find', icon: Document,
          component: () => import('../views/ui/find/index.vue')
        },
        {
          menuId: '1223', title: '列表控件', path: 'grid', icon: Document,
          component: () => import('../views/ui/grid/index.vue')
        },
        {
          menuId: '1224', title: '操作按钮', path: 'button', icon: Document,
          component: () => import('../views/ui/button/index.vue')
        },
        {
          menuId: '1225', title: '增删改查', path: 'crud', icon: Document,
          component: () => import('../views/ui/crud/index.vue')
        },
        {
          menuId: '1226', title: '增删改查（多）', path: 'list-page', icon: Document,
          component: () => import('../views/ui/crud/listPage.vue')
        }
      ]
    }
  ]
 

})