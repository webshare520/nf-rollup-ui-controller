import { Document, FolderOpened } from '@element-plus/icons-vue'
// import { createRouter } from '@naturefw/ui-elp'
import { createRouter } from '../../lib-elp/main'

// import { createRouter } from '../../lib/router/router'

import type { IMenus } from '../../lib/types/90-router'

import home from '../views/home.vue'

// import list from '../views/data-list.vue'

// import list from '../views/plat/p02-table.vue'

// const baseUrl = (document.location.host.includes('.gitee.io')) ?
//  '/nf-rollup-state' :  ''

const url = import.meta.env.BASE_URL
console.log('url', url)
const baseUrl = url === '/' ? '': '' + url

export default createRouter({
  /**
   * 基础路径
   */
  baseUrl: baseUrl,
  /**
   * 首页
   */
  home: home,

  menus: [
    /*
    {
      menuId: '1',
      title: '各种属性',
      naviId: '0',
      path: 'props',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '1010',
          title: '表单控件',
          path: 'form',
          icon: Document,
          component: () => import('../views/form/10-form.vue')
        },
        {
          menuId: '1020',
          title: '查询控件',
          path: 'find',
          icon: Document,
          component: () => import('../views/find/10-find.vue')
        },
        {
          menuId: '1060',
          title: '列表控件',
          path: 'grid',
          icon: Document,
          component: () => import('../views/grid/10-grid.vue')
        } 
      ]
    },
    */
    {
      menuId: '2000',
      title: '测试组件',
      naviId: '0',
      path: 'comp',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '2001',
          title: '基础控件',
          path: 'base',
          icon: Document,
          component: () => import('../views/comp/base/01-text.vue')
        },
        {
          menuId: '2002',
          title: '表单子控件',
          path: 'form-item',
          icon: Document,
          component: () => import('../views/comp/form-item/10-item.vue')
        },
        {
          menuId: '2010',
          title: '表单控件',
          path: 'form',
          icon: Document,
          component: () => import('../views/comp/form/10-form.vue')
        },
        {
          menuId: '2012',
          title: '表单控件的扩展',
          path: 'form-slot',
          icon: Document,
          component: () => import('../views/comp/form/20-form-slot.vue')
        },
        {
          menuId: '2014',
          title: '分栏的表单控件',
          path: 'form-card',
          icon: Document,
          component: () => import('../views/comp/form/30-form-card.vue')
        },
        {
          menuId: '2016',
          title: 'tabs 的表单',
          path: 'form-tabs',
          icon: Document,
          component: () => import('../views/comp/form/40-form-tabs.vue')
        },
        {
          menuId: '2018',
          title: 'step 的表单',
          path: 'form-step',
          icon: Document,
          component: () => import('../views/comp/form/50-form-step.vue')
        }
       
        /*    */
      ]
    },
    {
      menuId: '3000',
      title: '列表',
      naviId: '0',
      path: 'list',
      icon: FolderOpened,
      childrens: [
        {
          menuId: '3060',
          title: '列表控件',
          path: 'grid',
          icon: Document,
          component: () => import('../views/comp/grid/10-grid.vue')
        },
        {
          menuId: '3070',
          title: '列表控件的扩展',
          path: 'grid-slot',
          icon: Document,
          component: () => import('../views/comp/grid/20-grid-slot.vue')
        } ,
        {
          menuId: '3080',
          title: '行内编辑的列表',
          path: 'grid-edit',
          icon: Document,
          component: () => import('../views/comp/grid/30-grid-edit.vue')
        } 
      ]
    }
  ]
})
