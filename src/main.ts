import { createApp } from 'vue'
import App from './App.vue'

// UI库
import ElementPlus from 'element-plus'
// import 'element-plus/lib/theme-chalk/index.css'
// import 'dayjs/locale/zh-cn'
// import locale from 'element-plus/lib/locale/lang/zh-cn'
import zhCn from 'element-plus/es/locale/lang/zh-cn'

// 实现拖拽的自定义指令
// import { nfElementPlus } from '@naturefw/ui-elp'

// 简易路由
import router from './router/index'

// 设置icon
import installIcon from './icon/index'

// import axios from 'axios'

// 设置访问后端API
// serviceConfig.baseUrl = '/api/'
// serviceConfig.axios = axios

// import dayjs from 'dayjs'
// import { plugin } from '../node_modules/dayjs/plugin/isoWeek.d.ts'

// dayjs.extend(plugin)

const app = createApp(App)
app.config.globalProperties.$ELEMENT = {
  locale: zhCn,
  size: 'small'
}

app.use(router) // 路由
  .use(installIcon)
  .use(ElementPlus, { locale: zhCn, size: 'small' }) // UI库
  // .use(nfElementPlus) // 全局注册
  .mount('#app')
