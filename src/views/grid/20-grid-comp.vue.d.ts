declare const _default: import("vue").DefineComponent<{
    gridMeta: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridMeta>;
    };
    gridTable: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridTable>;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<{
            [key: string]: import("../../../lib-elp/map").IGridItem;
            [key: number]: import("../../../lib-elp/map").IGridItem;
        }>;
    };
    selection: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    dataList: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
}, {
    props: Readonly<import("@vue/shared").LooseRequired<Readonly<import("vue").ExtractPropTypes<{
        gridMeta: {
            type: import("vue").PropType<import("../../../lib-elp/map").IGridMeta>;
        };
        gridTable: {
            type: import("vue").PropType<import("../../../lib-elp/map").IGridTable>;
            default: boolean;
        };
        itemMeta: {
            type: import("vue").PropType<{
                [key: string]: import("../../../lib-elp/map").IGridItem;
                [key: number]: import("../../../lib-elp/map").IGridItem;
            }>;
        };
        selection: {
            type: import("vue").PropType<import("../../../lib-elp/map").IGridSelection>;
            default: () => {
                dataId: string;
                row: {};
                dataIds: never[];
                rows: never[];
            };
        };
        dataList: {
            type: import("vue").PropType<any[]>;
            default: () => never[];
        };
    }>> & {
        [x: string & `on${string}`]: ((...args: any[]) => any) | ((...args: unknown[]) => any) | undefined;
    }>>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    gridMeta: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridMeta>;
    };
    gridTable: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridTable>;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<{
            [key: string]: import("../../../lib-elp/map").IGridItem;
            [key: number]: import("../../../lib-elp/map").IGridItem;
        }>;
    };
    selection: {
        type: import("vue").PropType<import("../../../lib-elp/map").IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    dataList: {
        type: import("vue").PropType<any[]>;
        default: () => never[];
    };
}>>, {
    selection: import("../../../lib-elp/map").IGridSelection;
    dataList: any[];
    gridTable: import("../../../lib-elp/map").IGridTable;
}>;
export default _default;
