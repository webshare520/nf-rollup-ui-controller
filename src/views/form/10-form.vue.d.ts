declare const _default: import("vue").DefineComponent<{
    moduleID: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
}, {
    formMeta: {
        moduleId: number;
        formId: number;
        formColCount: number;
        colOrder: number[];
        formColShow: {
            "90": {
                "1": number[];
                "2": number[];
                "3": number[];
                "4": number[];
            };
        };
        ruleMeta: {
            "101": ({
                trigger: string;
                message: string;
                required: boolean;
                min?: undefined;
                max?: undefined;
            } | {
                trigger: string;
                message: string;
                min: number;
                max: number;
                required?: undefined;
            })[];
        };
        itemMeta: {
            "90": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            "100": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "101": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "102": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "105": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "110": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "111": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "112": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "113": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "114": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "115": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "116": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "120": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "121": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "150": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "151": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: boolean;
                placeholder: string;
                title: string;
                colCount: number;
            };
            "152": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            "153": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: string;
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            "160": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: number;
                placeholder: string;
                title: string;
                optionList: {
                    value: number;
                    label: string;
                }[];
                colCount: number;
            };
            "161": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                colCount: number;
            };
            "162": {
                columnId: number;
                colName: string;
                label: string;
                controlType: number;
                isClear: boolean;
                defValue: never[];
                placeholder: string;
                title: string;
                colCount: number;
                optionList: ({
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                        children: {
                            value: string;
                            label: string;
                        }[];
                    }[];
                } | {
                    value: string;
                    label: string;
                    children: {
                        value: string;
                        label: string;
                    }[];
                })[];
            };
        };
    };
    props: Readonly<import("@vue/shared").LooseRequired<Readonly<import("vue").ExtractPropTypes<{
        moduleID: {
            type: (StringConstructor | NumberConstructor)[];
            default: number;
        };
    }>> & {
        [x: string & `on${string}`]: ((...args: any[]) => any) | ((...args: unknown[]) => any) | undefined;
    }>>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    moduleID: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
}>>, {
    moduleID: string | number;
}>;
export default _default;
