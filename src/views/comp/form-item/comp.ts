
import { ref } from 'vue'

// 对象
const comp = {
  template: '我是模板：{{foo}}',
  props:{
    name: String
  },
  setup(props: object) {
    const foo = ref('我是对象')
    console.log('对象组件的 props', props)

    return {
      foo
    }
  }
}

// class
class BaseComp {
  template: string
  props: object
  setup: (props: object, ctx: object) => object

  constructor() {
    this.template = '我是模板：{{foo}}--{{name}}',
    this.props = {
      name: String
    },
    this.setup = (props) => {
      const foo = ref('我是 class')
      console.log('class 的 props', props)
      return {
        foo
      }
    }
  }
}

export {
  comp,
  BaseComp
}