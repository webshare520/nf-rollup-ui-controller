
import { reactive, onMounted, onUnmounted } from 'vue'

const active = reactive({
  moduleId: 0,
  oldModuleId: 0
})

export default function (modId) {

  // 记录原来的模块ID
  active.oldModuleId = active.moduleId
  // 记录激活的模块ID
  active.moduleId = modId

  /**
   * 获取激活的模块ID
   * @returns 
   */
  const getActiveModuleId = () => {
    return active.moduleId
  }

  /**
   * 设置激活模块ID
   * @param {*} id 
   */
  const setActiveModuleId = (id) => {
    active.moduleId = id
  }

  /**
   * 卸载组件的时候恢复模块ID
   */
  onUnmounted(() => {
    active.moduleId = active.oldModuleId
  })

  return {
    active,
    getActiveModuleId,
    setActiveModuleId
  }
}