import type { Ref } from 'vue';
declare const _default: import("vue").DefineComponent<{
    moduleId: (StringConstructor | NumberConstructor)[];
    active: ObjectConstructor;
    colOrder: {
        type: import("vue").PropType<(string | number)[]>;
        default: () => never[];
    };
    findValue: {
        type: ObjectConstructor;
        default: () => void;
    };
    findArray: {
        type: ObjectConstructor;
        default: () => never[];
    };
    quickFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    allFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    customer: {
        type: ArrayConstructor;
        default: () => void;
    };
    customerDefault: {
        type: ArrayConstructor;
        default: number;
    };
    reload: {
        type: BooleanConstructor;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<import("../../../lib/types/30-form").IFormItem>;
        default: () => void; /** book的说明 */
    };
}, {
    year: Ref<string | number>;
    year2: Ref<string | number>;
    props: Readonly<import("@vue/shared").LooseRequired<Readonly<import("vue").ExtractPropTypes<{
        moduleId: (StringConstructor | NumberConstructor)[];
        active: ObjectConstructor;
        colOrder: {
            type: import("vue").PropType<(string | number)[]>;
            default: () => never[];
        };
        findValue: {
            type: ObjectConstructor;
            default: () => void;
        };
        findArray: {
            type: ObjectConstructor;
            default: () => never[];
        };
        quickFind: {
            type: ArrayConstructor;
            default: () => never[];
        };
        allFind: {
            type: ArrayConstructor;
            default: () => never[];
        };
        customer: {
            type: ArrayConstructor;
            default: () => void;
        };
        customerDefault: {
            type: ArrayConstructor;
            default: number;
        };
        reload: {
            type: BooleanConstructor;
            default: boolean;
        };
        itemMeta: {
            type: import("vue").PropType<import("../../../lib/types/30-form").IFormItem>;
            default: () => void; /** book的说明 */
        };
    }>> & {
        [x: string & `on${string}`]: ((...args: any[]) => any) | ((...args: unknown[]) => any) | undefined;
    }>>;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    moduleId: (StringConstructor | NumberConstructor)[];
    active: ObjectConstructor;
    colOrder: {
        type: import("vue").PropType<(string | number)[]>;
        default: () => never[];
    };
    findValue: {
        type: ObjectConstructor;
        default: () => void;
    };
    findArray: {
        type: ObjectConstructor;
        default: () => never[];
    };
    quickFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    allFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    customer: {
        type: ArrayConstructor;
        default: () => void;
    };
    customerDefault: {
        type: ArrayConstructor;
        default: number;
    };
    reload: {
        type: BooleanConstructor;
        default: boolean;
    };
    itemMeta: {
        type: import("vue").PropType<import("../../../lib/types/30-form").IFormItem>;
        default: () => void; /** book的说明 */
    };
}>>, {
    colOrder: (string | number)[];
    itemMeta: import("../../../lib/types/30-form").IFormItem;
    findValue: Record<string, any>;
    findArray: Record<string, any>;
    quickFind: unknown[];
    allFind: unknown[];
    customer: unknown[];
    customerDefault: unknown[];
    reload: boolean;
}>;
export default _default;
