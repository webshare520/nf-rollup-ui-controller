
// import * as Icons from "@element-plus/icons-vue"

// 引入需要的图标
import {
  CloseBold,
  Close,
  Plus,
  Star,
  UserFilled,
  Loading,
  Connection,
  Edit,
  FolderOpened
} from '@element-plus/icons-vue'

// 放到字典里
const dictIcon = {
  CloseBold,
  Close,
  Plus,
  Star,
  UserFilled,
  Loading,
  Connection,
  Edit,
  FolderOpened
}

const installIcon = (app: any) => {
  // 便于模板获取
  // app.config.globalProperties.$icon = dictIcon
  // 使用全部图标
  // app.config.globalProperties.$icon = Icons
  Object.keys(dictIcon).forEach(key => {
    app.component(key, dictIcon[key])
  })
}

export default installIcon