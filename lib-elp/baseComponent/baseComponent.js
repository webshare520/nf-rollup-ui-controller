
import { ref } from 'vue'

import { store } from '/nf-state'

export default class BaseComp {
  constructor (_info) {
    this.name = _info.name ?? 'baseName_' + new Date().valueOf()
    this.inheritAttrs = _info.inheritAttrs ??  false
    this.components = _info.components ?? {} // 注册共用组件
    this.props = _info.props ?? {} // 设置共用属性
    this.store = store // 设置状态，这样就不用每次都 import 了。
    _init()
  }
  /**
   * 
   * @param {*} props 
   * @param {*} ctx 
   * @returns 
   */
  _setup (props, ctx) {
     // 可以设置共用成员
    const baseRef = ref('基类setup定义的')

    // 可以设置生命周期的钩子

    return {
      store, // 返回状态
      baseRef // 返回成员
    }
  }
  /**
   * 内部函数，初始化
   */
  _init() {
    // 设置共用属性
    this.props.moduleId = {
      type: [Number, String],
      default: '0'
    }
    this.props.actionId = {
      type: [Number, String],
      default: '0'
    }
    this.props.dataId = {
      type: [Number, String],
      default: '0'
    }

  }
}