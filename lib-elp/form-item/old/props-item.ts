import type { PropType } from 'vue'

import type {
  IOptionList,
  IOptionTree,
  // IWebAPI,
  IFormItemProps
} from '../types/20-form-item'

/**
 * 【不用了】 基础控件的共用属性，即表单子控件的基础属性
 */
export const itemProps = {
  /**
   * * columnId：number | string，字段ID、控件ID
   * * colName：string，字段名称，控件使用 model 的哪个属性，多个字段名称用 “_” 分割
   * * controlType：number，EControlType，控件类型，表单控件据此加载对应的子控件
   * * defValue: any，子控件的默认值
   * * webAPI：IWebAPI，访问后端API的参数
   * * delay：Number，防抖的时间间隔，0：不用防抖。
   * * events：事件集合，用于防抖，IEventDebounce
   * * 
   */
  formItemMeta: {
    type: Object as PropType<IFormItemProps>,
    default: () =>  {return {}}
  },
  /**
   * optionList：IOptionList | IOptionTree，控件的备选项，单选、多选、等控件需要
   */
  optionList: {
    type: Object as PropType<Array<IOptionList | IOptionTree>>,
    default: () =>  {return []}
  },
  /**
   * // 表单的 model，可以整体传入，便于子控件维护字段值。
   */
  model: {
    type: Object
  },
  /**
   * 是否显示可清空的按钮，默认显示
   */
  clearable: {
    type: Boolean,
    default: true
  },
  /**
   * 浮动的提示信息，部分控件支持
   */
  title: {
    type: String,
    default: ''
  },
  /**
   * 组件尺寸
   */
  size: {
    type: String,
    default: ''
  }
}