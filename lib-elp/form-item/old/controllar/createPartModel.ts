
import type { IFormChildMetaList } from '../../main'

type IShowCol = {
  [key: number]: boolean
}

type anyObject = {
  [key: string | number]: any
}

/**
 * 创建新的局部 model
 * @param model 表单完整的 model
 * @param partModel 表单部分 的 model
 * @param itemMeta 表单子控件的meta
 * @param showCol 
 */
export default function createPartModel<T extends anyObject>(
    model: T,
    partModel: anyObject,
    itemMeta: IFormChildMetaList,
    showCol: IShowCol
  ) {
    // const partModel = props.partModel || {}
    // const model = props.model
    // const itemMeta = props.itemMeta
    // 清空原来的属性
    for (const key in partModel) { 
      delete partModel[key]
    }
    // 加入新的属性
    for (const key in showCol) {
      if (showCol[key]) {
        const colName = itemMeta[key].colName
        const arr = colName.split('_')
        if (arr.length === 1) {
          partModel[colName] = model[colName] || ''
        } else {
          partModel[colName] = model[colName] || ''
          arr.forEach((i: string) => {
            partModel[i] = model[i] || ''
          })
        }
      }
    }
  }
