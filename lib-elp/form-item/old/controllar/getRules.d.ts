import type { // 表单组件的属性
IRuleMeta } from '../../types/30-form';
import type { IFormItem } from '../../types/20-form-item';
/**
 * 创建表单的验证规则，把字段ID转换为字段名称
 * @param itemMeta 表单子控件的meta
 * @param ruleMeta 表单控件的验证规则
 * @returns
 */
export declare const getRules: (itemMeta: IFormItem, ruleMeta: IRuleMeta) => {};
