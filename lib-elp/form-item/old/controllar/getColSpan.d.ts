import type { IFormItemList } from '../../types/20-form-item';
/**
 *  处理一个控件占用几个 格子 的需求
 * @param columnsNumber 表单控件的列数
 * @param itemMeta 表单子控件的 meta
 * @returns
 */
export declare const getColSpan: (columnsNumber: number, itemMeta: IFormItemList) => {
    formColSpan: {
        [x: number]: number;
    };
    setFormColSpan: (num?: number) => void;
};
