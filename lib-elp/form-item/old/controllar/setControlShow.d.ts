import type { IFromMeta } from '../../types/30-form';
import type { IFormItemList } from '../../types/20-form-item';
/**
 * 设置备选项和子控件的联动
 * @param formMeta 表单控件的meta
 * @param model 表单完整的 model
 * @param partModel 表单部分 的 model
 * @returns
 */
export declare const setControlShow: (formMeta: IFromMeta, itemMeta: IFormItemList, model: any, partModel: any) => {
    showCol: {
        [x: string]: boolean;
        [x: number]: boolean;
    };
    setFormColShow: () => void;
};
