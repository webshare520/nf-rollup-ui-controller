
export {

  serviceConfig, // 配置
  service, // 访问后端API
  lifecycle, // 生命周期

  // 简易路由
  createRouter,
  useRouter,
  
  config, // 设置表单需要的子控件
  
  nfElementPlus, // 安装插件
  install,
  loading, // 加载动画

  // 菜单
  nfMenu,
  // 路由视图
  nfRouterView,
  nfRouterViewTabs,
  // 表单
  nfForm, // 表单控件
  nfFormCard, // 分 card 的表单控件
  nfFormTab, // 分 tab 的表单控件
  nfFormStep, // 分步骤 的表单
  AllFormItem, // 表单子控件集合
  formItemKey, // id转换为控件
  // 查询
  nfFind, // 查询控件
  // 列表
  createDataList,
  nfGrid, // 列表控件
  nfGridSlot,
  // 操作按钮
  nfButton,

  // 表单
  nfFormSlot,
  itemProps,
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController, // 表单控件的控制类
  
  // 拖拽
  dialogDrag, // 拖拽对话框
  _gridDrag,
  _formDrag, // 表单
  // findDrag,
  formDrag,
  gridDrag
} from './main'