declare const _default: import("vue").DefineComponent<{
    naviId: {
        type: (StringConstructor | NumberConstructor)[];
        default: string;
    };
}, {
    domMenu: import("vue").Ref<null>;
    menus: never[];
    select: (index: any, indexPath: any) => void;
}, unknown, {}, {}, import("vue").ComponentOptionsMixin, import("vue").ComponentOptionsMixin, Record<string, any>, string, import("vue").VNodeProps & import("vue").AllowedComponentProps & import("vue").ComponentCustomProps, Readonly<import("vue").ExtractPropTypes<{
    naviId: {
        type: (StringConstructor | NumberConstructor)[];
        default: string;
    };
}>>, {
    naviId: string | number;
}>;
export default _default;
