
export {
  service, // 访问后端API

  createDataList,
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController // 表单控件的控制类
} from './map'

export {
  AllFormItem,
  formItemKey
} from './form-item/_map-form-item'

