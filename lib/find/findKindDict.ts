
/**
 * 查询方式
 * * 数字 =、<>、>=、<=、between
 * * 字符串 =、不等于、包含、起始于、结束于
 * * 日期时间 =、between
*/
export const findKindDict = {
  // 字符串
  21: { id: 21, name: ' = ', where: '{col} = ?' }, // 等于，精确查询。+ 数字、日期
  22: { id: 22, name: ' ≠ ', where: '{col} <> ?' }, // 不等于，精确查询。+ 数字、日期
  23: { id: 23, name: '包含', where: '{col} like "%?%"' }, // 包含，模糊查询
  24: { id: 24, name: 'no包含', where: '{col} not like "%?%"' }, // 不包含，模糊查询
  25: { id: 25, name: '起始于', where: '{col} like "?%"' }, // 起始于，模糊查询
  26: { id: 26, name: '结束于', where: '{col} like "%?"' }, // 结束于，模糊查询
  27: { id: 27, name: 'no起始', where: '{col} not like "?%"' }, // 起始于，模糊查询
  28: { id: 28, name: 'no结束', where: '{col} not like "%?"' }, // 结束于，模糊查询
  // 数字
  // 11: { id: 11, name: ' = ', where: '{col} = ?'  }, // 等于 数字
  // 12: { id: 12, name: ' ≠ ', where: '{col} <> ?'  }, // 不等于 数字
  13: { id: 13, name: ' > ', where: '{col} > ?' }, // 大于 数字
  14: { id: 14, name: ' ≥ ', where: '{col} >= ?' }, // 大于等于 数字
  15: { id: 15, name: ' < ', where: '{col} < ?' }, // 小于 数字
  16: { id: 16, name: ' ≤ ', where: '{col} <= ?' }, // 小于等于 数字
  17: { id: 17, name: '从', where: '{col} between ? and ?' }, // 范围查询，闭区间
  18: { id: 18, name: '< X ≤', where: '{col} > ? and {col} <= ?' }, // 范围查询，开闭区间
  19: { id: 19, name: '≤ X <', where: '{col} >= ? and {col} < ?' }, // 范围查询，闭开区间
  // 日期、时间
  // 31: { id: 31, name: ' = ', where: '{col} = ?'  }, // 等于 日期、时间
  // 32: { id: 32, name: ' ≠ ', where: '{col} <> ?'  }, // 不等于 日期、时间
  // 33: { id: 33, name: ' > ', where: '{col} > ?'  }, // 大于 日期、时间
  // 34: { id: 34, name: ' >= ', where: '{col} >= ?'  }, // 大于等于 日期、时间
  // 35: { id: 35, name: ' < ', where: '{col} <  ?'  }, // 小于 日期、时间
  // 36: { id: 36, name: ' <= ', where: '{col} <= ?'  }, // 小于等于 日期、时间
  // 37: { id: 37, name: '从', where: '{col} between ? and ?'  }, // 范围 日期、时间
  // 多选
  40: { id: 40, name: ' 包含 ', where: '( {col} like %?% or {col} like %?% ... ) ' }, // 集合查询，在集合内
  41: { id: 41, name: ' 包含 ', where: '{col} in (?)' }, // 集合查询，在集合内
  42: { id: 42, name: ' 不包含 ', where: '{col} not in (?)' } // 集合查询，不在集合内
}
