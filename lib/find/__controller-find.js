import { ref, onMounted, reactive, watch } from 'vue'
import { typeName } from '@naturefw/nf-tool'
// 创建查询用的结构体
import { createFindModel } from './controller-model.js'

import mykeydown from '../key/keydown.js'

/**
 * 判断有没有查询值
 * @param val 查询值
 * @returns true：没有查询值；false：有查询值
 *  'function',
  * *   'async',
  * *   'object',
  * *   'array',
  * *   'string',
  * *   'number',
  * *   'bigInt',
  * *   'regExp',
  * *   'date',
  * *   'map',
  * *   'set',
  * *   'promise',
  * *   'symbol',
  * *   'math',
  * *   'null',
  * *   'undefined'
 */
const _isEmpty = (val) => {
  switch (typeName(val)) {
    case 'null':
    case 'undefined':
      return true
      break
    case 'string':
      return val.length === 0
      break
    case 'number':
    case 'bigInt':
      return isNaN(val)
      break
    case 'array':
      return val.length === 0
      break
    default:
      return false
      break
  }
}

/**
 * 处理多级联动的查询结果
 * @param colName 字段名称
 * @param findModel 查询结构，完整
 * @param findValue 简洁型的查询结果
 * @returns 返回标准结构，数组形式，多个
 */
const _selectCascader = (colName, findModel, findValue) => {

  // 标准模式
  const reArray = []
  // 简洁模式
  const colNames = colName.split(',')
  let index = 0
  colNames.forEach(col => {
    if (index < findModel.value.length) {
      reArray.push({
        colName: col,
        findKind: findModel.useKind,
        value: findModel.value[index]
      })
      findValue[col] = [findModel.useKind, findModel.value[index] ]
    } else {
      delete findValue[col]
    }
    index++
  })
  return reArray
}

const _selectCascaderDelete = (colName, findValue) => {
  // 简洁模式
  const colNames = colName.split(',')
  let index = 0
  colNames.forEach(col => {
    delete findValue[col]
  })
}

/**
 * 查询控件的控制类
 * @param props 组件的属性
 * @returns 
 */
export default function findController (props) {
  // 获取 $ref
  const formControl = ref(null)
  
  onMounted(() => {
  })

  const resetForm = () => {
    // 清空表单
    formControl.value.resetFields()
  }

  const dialogInfo = reactive({
    isShow: false
  })

  const moreOpen = () => {
    dialogInfo.isShow = true
  }

  // 查询控件 的内部结构
  const findModel = createFindModel(props.itemMeta)
  // 快捷查询的控件ID集合
  const arrQuickFind = reactive(props.quickFind)

  // 返回给父组件的查询结果
  const findValue = props.findValue // 简洁型，对象
  const findArray = props.findArray // 标准型，数组
  // 查询子控件的 meta
  const itemMeta = props.itemMeta

  // 按钮快捷键
  mykeydown('b', (e, isAlt) => {
    if (isAlt && props.active.moduleId === props.moduleId) {
      dialogInfo.isShow = true
    }
  })

  // 监听整体，给局部赋值
  watch(findModel, () => {
    if (dialogInfo.isShow) {
      // 设置快捷查询的字段
      arrQuickFind.length = 0
    }
    
    // 赋值，提交给调用者。两种格式，一个是标准型，一个是简约型
    findArray.length = 0
    for(const key in findModel) {
      const colName = itemMeta[key].formItemMeta.colName
      const colId = itemMeta[key].formItemMeta.columnId
      const controlType = itemMeta[key].formItemMeta.controlType
      const find = findModel[key]
      if (_isEmpty(find.value)) {
        // 没有查询值，删除
        if (controlType === 162) {
          // 联动下拉
          _selectCascaderDelete(colName, findValue)
        } else {
          delete findValue[colName]
        }
      } else {
        if (dialogInfo.isShow) {
          // 显示到快捷
          arrQuickFind.push(colId)
        }
        // 设置查询条件
        if (find.useKind >= 417 && find.useKind <= 419) {
          // 范围查询
          findValue[colName] = [find.useKind, [find.value, find.value2]]
          findArray.push({
            colName: colName,
            findKind: find.useKind,
            valueStart: find.value,
            valueEnd: find.value2
          })
        } else {
          if (controlType === 162) {
            // 联动下拉
            findArray.push(..._selectCascader(colName, find, findValue))
          } else {
            // 其他
            findValue[colName] = [find.useKind, find.value ]
            findArray.push({
              colName: colName,
              findKind: find.useKind,
              value: find.value
            })
          }
        }
      }
    }
 
  })

  return {
    resetForm, // 重置表单
    moreOpen, // 打开更多
    dialogInfo, // 是否显示更多
    findModel, // 内部查询结构
    arrQuickFind, // 快捷查询
    formControl // 获取表单的dom
  }
}
