/**
 * 生命周期的钩子函数，显示一些状态，调试用
 */
/**
 * _flag：组件标记
 * isTime：是否计算用时
 * _events：父组件的回调函数
 */
export default function lifecycle(_flag: any, isTime?: boolean, _events?: {}): void;
