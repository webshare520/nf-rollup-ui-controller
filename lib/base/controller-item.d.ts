import type { IFormItemMeta } from '../types/20-form-item';
interface IProps {
    model: any;
    formItemMeta: IFormItemMeta;
    modelValue: any;
}
/**
 * 表单子控件的控制函数
 * @param props 组件的 props
 * @param emit 组件的 emit
 * @returns
 */
export default function itemController<T extends IProps, U>(props: T, emit: (event: U, ...args: any[]) => void, events?: any): {
    value: any;
    run: () => void;
    clear: (e: any) => void;
};
export {};
