// 级联下拉，数据类型结构的转换函数

// 省市区县级联的情况
const foo = {
  levelName: ['foo1', 'foo2', 'foo3'],
  foo1: [
    { value: 'zhinan', label: '指南' },
    { value: 'zujian', label: '组件' },
    { value: 'ziyuan', label: '资源' }
  ],
  foo2: [
    { parentId: 'zhinan', value: 'shejiyuanze', label: '设计原则' },
    { parentId: 'zhinan', value: 'daohang', label: '导航'},
    { parentId: 'zujian', value: 'basic', label: 'basic'},
    { parentId: 'zujian', value: 'form', label: 'form'}
  ],
  foo3: [
    { parentId: 'shejiyuanze', value: 'yizhi', label: '一致'},
    { parentId: 'shejiyuanze', value: 'fankui', label: '反馈'}
  ]
}

// 组织架构的情况
const foo2 = [
  { parentId: '', value: 'zhinan', label: '指南' },
  { parentId: '', value: 'zujian', label: '组件' },
  { parentId: '', value: 'ziyuan', label: '资源' },
  { parentId: 'zhinan', value: 'shejiyuanze', label: '设计原则' },
  { parentId: 'zhinan', value: 'daohang', label: '导航'},
  { parentId: 'zujian', value: 'basic', label: 'basic'},
  { parentId: 'zujian', value: 'form', label: 'form'},
  { parentId: 'shejiyuanze', value: 'yizhi', label: '一致'},
  { parentId: 'shejiyuanze', value: 'fankui', label: '反馈'}
]

// 获取子节点，一个数组
const getChildrenOne = (data, parentId, level) => {
  const arr = []
  // 查找子集
  const opts = data.filter(item => item.parentId == parentId)
  // 添加子集
  for (let i = 0; i < opts.length; i++) {
    // 遍历arr
    arr.push({
      value: opts[i].value,
      label: opts[i].label,
      leaf: false,
      children: [...getChildrenOne(data, opts[i].value, level + 1)]
    })
    if (arr[arr.length - 1].children.length === 0) {
      // 没有子节点
      // arr[arr.length-1].leaf = true
      delete arr[arr.length - 1].children
      arr[arr.length - 1].leaf = true
    }
  }
  return arr
}

// 获取子节点，多个数组
const getChildren = (data, parentId, level) => {
  const arr = []
  // 记录最大层数
  const maxLevel = data.levelName.length

  if (maxLevel === level) {
    // 没有下一级了
    return arr
  } else {
    // 记录本级的名称
    const levelName = data.levelName[level]
    // 依据父ID查找子集
    const opts = data[levelName].filter(item => item.parentId === parentId)

    // 遍历添加
    for (let i = 0; i < opts.length; i++) {
      // 遍历arr
      arr.push({
        value: opts[i].value,
        label: opts[i].label,
        leaf: false,
        children: [...getChildren(data, opts[i].value, level + 1)]
      })
      // 没有children ，删除
      if (arr[arr.length - 1].children.length === 0) {
        // 没有子节点
        delete arr[arr.length - 1].children
        arr[arr.length - 1].leaf = true
      }
    }
    return arr
  }
}

// 递归，浅层转嵌套
const shallowToDeep = (data) => {
  const arr = []
  // 记录每一级的名称
  const levelName = data.levelName
  const opts = data[levelName[0]]
  for (let i = 0; i < opts.length; i++) {
    arr.push({
      value: opts[i].value,
      label: opts[i].label,
      leaf: false,
      children: [...getChildren(data, opts[i].value, 1)]
    })
    if (arr[arr.length - 1].children.length === 0) {
      delete arr[arr.length - 1].children
      arr[arr.length - 1].leaf = true
    }
  }
  return arr
}

const cascaderManage = (data) => {
  // let id = 0

  const levelName = data.levelName
  // const options = []

  // 级联需要的属性 props
  const propsCascader = {
    // multiple: false, // 单选还是多选 多选返回 value 的二维数组
    lazy: false, // 动态级联
    lazyLoad (node, resolve) { // 响应函数
      const { level } = node
      // 判断级数是否超过数组下标
      if (levelName.length >= level) {
        // 找到子数据-
        const key = levelName[level]
        const newNode = data[key].filter((item) => item.parentId === node.value)
        resolve(newNode) // 交给组件
      } else {
        resolve([{
          value: '22',
          label: '选项11',
          leaf: true
        }])
      }
    }
  }

  return {
    propsCascader
  }
}

export {
  getChildrenOne,
  getChildren,
  shallowToDeep,
  cascaderManage
}