import type { IEventDebounce } from '../../types/20-form-item';
/**
 * 控件的防抖输入
 * @param props 组件的 props
 * @param emit 组件的 emit
 * @param key v-model的名称，默认 modelValue，用于emit
 * @param events 事件集合，run：立即提交；clear：清空计时，用于汉字输入
 * @param delay 延迟时间，默认500毫秒
 */
export default function debounceRef<T, K extends keyof T>(props: T, emit: (event: any, ...args: any[]) => void, key: K, events: IEventDebounce, delay?: number): import("vue").Ref<T[K]>;
