/**
 * 控件的直接输入，不需要防抖。负责父子组件交互表单值。
 * @param model 组件的 props 的 model
 * @param colName 需要使用的属性名称
 */
export default function modelRef<T, K extends keyof T>(model: T, colName: K): import("vue").WritableComputedRef<T[K]>;
