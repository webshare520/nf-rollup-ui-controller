interface IModel {
    [key: string | number]: any;
}
/**
 * 一个控件对应多个字段的情况，不支持 emit
 * @param model 表单的 model
 * @param arrColName 使用多个属性，数组
 */
export default function rangeRef<T extends IModel>(model: T, arrColName: Array<string | number>): import("vue").Ref<any[]>;
export {};
