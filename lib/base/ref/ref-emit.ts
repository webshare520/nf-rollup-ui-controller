import { customRef } from 'vue'
// import type { ObjectEmitsOptions } from 'vue'

/**
 * 控件的直接输入，不需要防抖。负责父子组件交互表单值
 * @param props 组件的 props
 * @param emit 组件的 emit
 * @param key v-model 的名称，默认 modelValue，用于 emit
 */
export default function emitRef<T, K extends keyof T & string>
(
  props: T,
  emit: (event: any, ...args: any[]) => void, // "update:modelValue" string
  key: K
) {

  return customRef<T[K]>((track: () => void, trigger: () => void) => {
    return {
      get(): T[K] {
        track()
        return props[key] // 返回 modelValue 的值
      },
      set(val: T[K]) {
        trigger()
        // 通过 emit 设置 modelValue 的值
        emit(`update:${key.toString()}`, val) 
      }
    }
  })
}
