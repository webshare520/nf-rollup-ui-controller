/**
 * 控件的直接输入，不需要防抖。负责父子组件交互表单值
 * @param props 组件的 props
 * @param emit 组件的 emit
 * @param key v-model 的名称，默认 modelValue，用于 emit
 */
export default function emitRef<T, K extends keyof T & string>(props: T, emit: (event: any, ...args: any[]) => void, // "update:modelValue" string
key: K): import("vue").Ref<T[K]>;
