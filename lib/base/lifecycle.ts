/**
 * 生命周期的钩子函数，显示一些状态，调试用
 */

import {
  // onBeforeCreate,
  // onCreated,
  onBeforeMount,
  onMounted,
  onBeforeUpdate,
  onUpdated,
  onBeforeUnmount,
  onUnmounted,
  onErrorCaptured,
  onRenderTracked,
  onRenderTriggered,
  onActivated,
  onDeactivated,
  provide,
  inject,
  nextTick
} from 'vue'

import { log, logTime } from '@naturefw/nf-tool'

const f = Symbol('___nf-liefcycle___')
const flagCallback = Symbol('___nf-liefcycle-callback___')

/**
 * _flag：组件标记
 * isTime：是否计算用时
 * _events：父组件的回调函数
 */
export default function lifecycle (_flag, isTime = false, _events = {}) {

  // 获取父组件的回调事件
  const events = inject(flagCallback) ?? {}

  // 如果有回调事件，存入 provide 以便子组件调用
  provide(flagCallback, _events)

  // 加载动画
  if(typeof events.start === 'function') events.start()

  // 判断上级的标识
  const _parentflag = inject(f)
  const flag = (_parentflag) ?
    _parentflag + '----' + _flag :
    _flag

  // 记入标记
  provide(f, flag)

  // 判断是否计时
  const t = (isTime) ? 
    logTime(`${flag}——【加载】用时`) :
    {end: () => {} }

  let t2 = null

  // 在实例初始化之后、进行数据侦听和事件/侦听器的配置之前同步调用。
  // onBeforeCreate((res) => {
  //  log(`${flag} == onBeforeCreate ：`, res)
  // })

  // 在实例创建完成后被立即同步调用
  // onCreated((res) => {
  //  log(`${flag} == onCreated ：`, res)
  // })

  // 在挂载开始之前被调用
  onBeforeMount(() => {
    // log(`${flag} == onBeforeMount`)
  })

  // 在实例挂载完成后被调用
  onMounted(() => {
    // log(`${flag} == onMounted`)
    t.end()
    if(typeof events.end === 'function') {
      nextTick(() => {
        events.end()
      })
    }
  })

  // 在数据发生改变后，DOM 被更新之前被调用
  onBeforeUpdate(() => {
    // log(`${flag} == onBeforeUpdate ：`)
    if (isTime) {
      t2 = logTime(`${flag}——【更新】 用时`)
    } else {
      t2 = null
    }
  })

  // 在数据更改导致的虚拟 DOM 重新渲染和更新完毕之后被调用
  onUpdated(() => {
    // log(`${flag} == onUpdated`)
    if (isTime) t2.end()
  })

  // 在卸载组件实例之前调用
  onBeforeUnmount(() => {
    // log(`${flag} == onBeforeUnmount`)
    if (isTime) {
      t2 = logTime(`${flag}——【卸载】用时`)
    } else {
      t2 = null
    }
  })

  // 卸载组件实例后调用
  onUnmounted(() => {
    // log(`${flag} == onUnmounted`)
    if (isTime) t2.end()
  })

  // 在捕获一个来自后代组件的错误时被调用
  onErrorCaptured((res) => {
    log(`${flag} == onErrorCaptured`, res)
  })

  // 跟踪虚拟 DOM 重新渲染时调用
  onRenderTracked((res) => {
    // log(`${flag} == onRenderTracked ：`, res)
  })

  // 当虚拟 DOM 重新渲染被触发时调用
  onRenderTriggered((res) => {
    // log(`${flag} == onRenderTriggered ：`, res)
  })

  // 被 keep-alive 缓存的组件激活时调用
  onActivated((res) => {
    log(`${flag} == onActivated`, res)
  })

  // 被 keep-alive 缓存的组件失活时调用
  onDeactivated((res) => {
    log(`${flag} == onDeactivated`, res)
  })
}