import { onMounted, onUnmounted } from 'vue'

const _keyDict = {
  Home: 'h', // 首页
  f: 'h', // 首页
  End: 'e', // 末页
  e: 'e',  // 末页
  w: '-', // 向前翻页
  a: '-', // 向前翻页
  s: '+', // 向后翻页
  d: '+', // 向后翻页
  ArrowLeft: '-', // 向前翻页
  ArrowRight: '+', // 向后翻页
  ArrowUp: '-', // 向前翻页
  ArrowDown:'+' // 向后翻页

}
/**
 * 键盘按键的监听，监听一个按键
 * @param {*} pagerInfo 分页信息
 * * size: 10, // 一页记录数
 * * total: 200, // 总数
 * * count: 20, // 页数
 * * index: 1 // 当前页号
 * @param {*} dict 按键的对应关系
 */
export default function mykeypager(pagerInfo, dict = {}) {
  // 防抖输入
  let oldKey = ''
  let timeOut = null

  const keyDict = {}
  Object.assign(keyDict, _keyDict, dict)

  // 方向键翻页，包含home、end等
  const _movePager = (key) => {
    switch (key) {
      case 'h':
        pagerInfo.index = 1
        break
      case 'e':
        pagerInfo.index = pagerInfo.count
        break
      case '-':
        if (pagerInfo.index > 1) {
          pagerInfo.index -= 1
        }
        break
      case '+':
        if (pagerInfo.index < pagerInfo.count) {
          pagerInfo.index += 1
        }
        break
    }
  }

  // 防抖的数字翻页
  const _debouncePager = (key) => {
    // 快捷数字键，做防抖，可以输入 > 9 的页号
    oldKey += key
    clearTimeout(timeOut)
    timeOut = setTimeout(() => {
      const tmp = parseInt(oldKey)
      if (tmp <= 1){
        pagerInfo.index = 1
      } else if (tmp <= pagerInfo.count){
        pagerInfo.index = tmp
      } else {
        pagerInfo.index = pagerInfo.count
      }
      oldKey = ''
    }, 300)
  }

  // 数字键，不防抖，1-9。0:10
  const _numPager = (key) => {
    const tmp = key === '0' ? 10 : parseInt(key)
    if (tmp <= pagerInfo.count) {
      pagerInfo.index = tmp
    }
  }

  // 按下的事件
  const butKeydown = (event) => {
    const re = Object.prototype.toString.call(event.target) === '[object HTMLBodyElement]'
    if (re) { // 来自于 body
      if (event.code.includes('Num')) {
        // 防抖，合并翻页
        _debouncePager(event.key)
      } else if (event.code.includes('Dig')){
        // 数字键，不防抖，1-9。0:10
        _numPager(event.key)
      } else {
        // 方向键翻页
        _movePager(keyDict[event.key])
      }
    }
  }

  // 监听事件
  onMounted((info) => {
    // 创建
    document.addEventListener("keydown", butKeydown)
    // document.addEventListener("keyup", butKeyup)
  })
 
  // 撤销监听
  onUnmounted((info) => {
    // 卸载
    document.removeEventListener("keydown", butKeydown)
    // document.removeEventListener("keyup", butKeyup)
  })
}