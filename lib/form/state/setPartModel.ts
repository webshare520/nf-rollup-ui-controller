
import type { IFormState, IAnyObject } from '../../main'
 
/**
 * 创建新的局部 model
 * @param state 表单控件的 状态
 */
export default function createPartModel<T extends IAnyObject>(
    state: IFormState<T>
  ) {
   
    const {
      model,
      partModel,
      showCol,
      childMetaList 
    } = state

    // 清空原来的属性
    for (const key in partModel) { 
      delete partModel[key]
    }
    // 加入新的属性
    for (const key in showCol) {
      if (showCol[key] && partModel) {
        const colName = childMetaList[key].colName
        const arr = colName.split('_')
        if (arr.length === 1) {
          partModel[colName] = model[colName] || ''
        } else {
          partModel[colName] = model[colName] || ''
          arr.forEach((i: string) => {
            partModel[i] = model[i] || ''
          })
        }
      }
    }
  }
