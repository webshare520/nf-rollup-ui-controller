/**
 * 表单控件的控制类
 */
import { watch } from 'vue'

import type {
  IAnyObject,
  IFromProps // 表单控件需要的 meta
} from '../main'

import useColSpan from './state/useColSpan'
import useLinkage from './state/useLinkage'

import {
  regFormState,
  getFormState
} from './state/stateForm'

/**
 * 表单控件的管理类，创建内部局部状态
 * @param props 表单控件的 props
 * @returns 
 */
export default function formController<T extends IAnyObject> (
  props: IFromProps<T>
) {

  // 注册一个状态，统一管理各种数据
  const state = regFormState(props)
  // IFormItemList<T>, 表单子组件的 meta
  const { childMetaList, formMeta } = state

  // 调用 useColSpan 设置一个字段占用多少 span， 返回刷新函数
  const { setFormColSpan } = useColSpan(state)
  
  // 调用 useLinkage 监听联动关系，返回刷新函数
  const { setFormColShow } = useLinkage(state)

  // 监听列数，变化后重新设置
  watch(() => formMeta.columnsNumber, (num) => {
    setFormColSpan(num)
  }, { immediate:true })

  // 监听控件的格子数，变化后重置
  watch(childMetaList, () => {
    setFormColSpan()
  })

  // 监听联动的变化，更新
  watch(formMeta.linkageMeta, () => {
    setFormColShow()
  })

  watch(formMeta.colOrder, () => {
    setFormColShow()
    setFormColSpan()
  })

  // 返回重置函数 
  formMeta.reset = () => {
    // alert('表单内部修改外部函数')
    setFormColShow()
  }

  return {
    state 
  }
}
