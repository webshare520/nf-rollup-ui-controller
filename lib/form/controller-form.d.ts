import type { IFromMeta } from '../types/30-form';
import type { IFormItemList } from '../types/20-form-item';
/**
 * 表单控件的管理类
 * * 创建 v-model，创建局部 model，设置行列、排序相关的处理
 * @param formMeta 表单控件的 meta
 * @param model 表单的 model
 * @param partModel 表单的部分 model
 * @returns
 */
export default function formController(formMeta: IFromMeta, itemMeta: IFormItemList, model: any, partModel?: any): {
    formColSpan: {
        [x: number]: number;
    };
    showCol: {
        [x: string]: boolean;
        [x: number]: boolean;
    };
    setFormColShow: () => void;
};
