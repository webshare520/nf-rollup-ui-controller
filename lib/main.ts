
// 配置
import serviceConfig from './config'
// 服务
import service from './service'

// 生命周期
import lifecycle from './base/lifecycle'

// 简易路由
import { createRouter, useRouter } from './router/router'

// 向上返回组件的事件
import { myExpose } from './base/comp-expose'
 
// 查询控件的属性
import { findProps } from './find/props-find'
// 列表控件的属性
import { gridProps } from './grid/props-grid'

// option 选项的管理类，包括级联的操作
import {
  getChildrenOne,
  getChildren,
  shallowToDeep,
  cascaderManage
} from './base/controller-option'

// 不用防抖的输入
import custmerRef from './base/ref/ref-model'
// 防抖输入
import debounceRef from './base/ref/ref-emit-debounce'
// 日期类型的输入
// import dateRef from './base/ref-date'

// emit
import emitRef from './base/ref/ref-emit'
import emitDebRef from './base/ref/ref-emit-debounce'

// model
import modelRef from './base/ref/ref-model'
import rangeRef from './base/ref/ref-model-range'
import range2Ref from './base/ref/ref-model-range2'
import modelDebRef from './base/ref/ref-model-debounce'

// 加载后端API的数据
import loadController from './base/controller-load'

// 表单子控件的管理类
import itemController from './base/controller-item'
// 创建表单需要的 v-model
import createModel from './form/state/createModel'
// 表单控件的控制类
import formController from './form/controller-form'

// 查询方式的字典
import { findKindDict } from './find/findKindDict'
// 创建查询需要的 内部存储结构
// import { createFindModel } from './find/controller-model'
// 查询控件的控制类
// import findController from './find/controller-find'

// 列表
import createDataList from './grid/controller-model'

// 监听按键
import mykeydown from './key/keydown'
import mykeypager from './key/keypager'

// 拖拽 dialog
import dialogDrag from './drag/dialogDrag'
// 设置 grid的拖拽
import gridDrag from './drag/gridDrag'

// 设置表单的拖拽
import formDrag from './drag/formDrag'

// 设置 td、div可以拖拽
import _domDrag from './drag/controller/domDrag'

// 套个函数，避免 new
const domDrag = (th, meta, dragInfo, colId) => new _domDrag(th, meta, dragInfo, colId)

// 表单控件的状态
import {
  regFormState,
  getFormState
} from './form/state/stateForm'

export {
  // 配置
  serviceConfig,
  // 访问后端API
  service,
  // 生命周期
  lifecycle,
  // 简易路由
  createRouter,
  useRouter,
  // 传声筒
  myExpose,
  // 属性
  // itemProps, // 表单子控件的共用属性
  // formProps, // 表单控件的属性
  findProps, // 表单控件的属性
  gridProps, // 列表控件的属性
  // 选项
  getChildrenOne,
  getChildren,
  shallowToDeep,
  cascaderManage, // 下拉列表框等的选项的控制类
  // 表单
  createModel, // 创建表单需要的 v-model
  loadController, // 加载后端字典
  itemController, // 表单子控件的控制类
  formController, // 表单控件的控制类
  // 表单状态
  regFormState,
  getFormState,
  // 查询
  findKindDict, // 查询方式的字典
  // createFindModel, // 创建查询需要的 内部存储结构
  // findController, // 查询控件的控制类
  // 列表
  createDataList, // 建立演示数据
  // 按键
  mykeydown, // 操作按钮
  mykeypager, // 翻页的按键
  // 拖拽
  dialogDrag, // 拖拽 dialog
  gridDrag, // 数据列表的拖拽设置
  formDrag, // 设置表单的拖拽
  domDrag, // 设置 td、div可以拖拽
  // 赋值
  emitRef,
  emitDebRef,
  modelRef,
  rangeRef,
  range2Ref,
  modelDebRef,
  debounceRef, // 防抖
  // dateRef, // 处理日期
  custmerRef // 不防抖
}

// 导出类型

export type {
  // './20-form-item'
  IAnyObject,
  IWebAPI, // 访问后端API的配置
  IOptionList, // 单层的选项，下拉列表等的选项。value、label
  IOptionTree, // 多级的选项，级联控件。value、label、children（n级嵌套）
  IEventDebounce, // 防抖相关的事件

  IFormChildPropsList, // 表单里 input 这类的 props 集合，含 meta
  IFormChildMetaList, // 表单里 input 这类的 meta 集合（纯）
  IFormChildOnlyPropsList, // 纯 input 的 props
  IFormChildMeta, // 表单里 input 这类的 meta
  IFormChildProps, // 表单里 input 这类的 props，含meta
  
  // './30-form'
  FormColSpan,
  ShowCol,
  IFormState,
  ILinkageMeta, // 显示控件的联动设置
  IRule, // 一条验证规则，一个控件可以有多条验证规则
  IRuleMeta, // 表单的验证规则集合
  ISubMeta, // 分栏表单的设置
  IFromMeta, // 低代码的表单需要的 meta
  IFromProps, // 表单控件的属性，约束必须有的属性
  
  // './35-form-item'
  IFormItemMeta,
  
  //'./40-find'
  ICusFind, // 自定义查询方式
  IQueryMini, // 紧凑型查询：IFindArray
  IQuery, // 标准型查询
  IFindProps, // 查询控件的属性
  IFindState, // 查询需要的状态，便于各个组件传递数据
  
 //'./50-grid'
  IGridMeta, // 列表的 meta
  IGridItem, // 列的属性
  IGridItemList, // 列表字段的列的属性
  IGridSelection, // 列表里选择的数据
  IGridProps, // 列表控件的属性的类型
  
  
  // './70-drag'
  IDragInfo, // 拖拽时记录相关信息
  IDragEvent, // 拖拽相关的事件

  // './90-router'
  IMenuList, // 多层的菜单
  IMenu, // 一个菜单的类型
  IMenus, // 创建路由-菜单时的对象类型
  ICurrentRoute, // 
  IRouter, // 路由的类型

} from './types/main'

export {
   // './enum'
   EControlType, // 控件类型的枚举
   EFindKind, // 查询方式的枚举
   EAlign, // 横向对齐方式，左、中、右
   ESize, //  控件尺寸，大中小
   ESubType // 表单控件的分栏类型。
 } from './types/main'