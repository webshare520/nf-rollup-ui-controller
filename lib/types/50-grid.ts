
import type { EAlign } from './enum'

// 列表控件 ===========================================================


/**
 * 列表的 meta，LowCode 需要
 * * moduleId: number | string,
 * * idName: String,
 * * colOrder: Array<number|string>
 */
export interface IGridMeta {
  /**
   * 模块ID，number | string
   */
  moduleId: number | string,
  /**
   * 主键字段的名称 String，对应 row-key
   */
  idName: string,
  /**
   * 列（字段）显示的顺序 Array<number|string>
   */
  colOrder: Array<number|string>
}

/**
 * 列的属性，基于 el-table-column
 * * id: number | string,
 * * colName: string, 字段名称
 * * label: string, 列的标签、标题
 * * width: number, 列的宽度
 * * align: EAlign, 内容对齐方式
 * * headerAlign: EAlign 列标题对齐方式
 * * 其他属性
 */
export interface IGridItem {
  /**
   * 字段ID、列ID
   */
  id: number | string,
  /**
   * 字段名称
   */
  colName: string,
  /**
   * 列的标签、标题
   */
  label: string,
  /**
   * 列的宽度
   */
  width: number,
  /**
   * 内容对齐方式
   */
  align: EAlign,
  /**
   * 列标题对齐方式
   */
  headerAlign: EAlign,

  /**
   * 其他扩展属性
   */
  [propName: string]: any
}

/**
 * 列表字段的列的属性
 */
export interface IGridItemList {
  [key:string | number]: IGridItem
}

/**
 * 列表里选择的数据
 */
export interface IGridSelection<T> {
  /**
   * 单选，记录主键ID值
   */
  dataId: number | string,
  /**
   * 单选，记录选择的 row
   */
  row: T,
  /**
   * 多选，记录ID集合
   */
  dataIds: Array<number | string>,
  /**
   * 多选，记录 row 的集合
   */
  rows: Array<T>
}

/**
 * 新，放弃对整体 props 设置类型
 * * 列表控件的属性的类型，
 * * 可以用于 script setup 的 props（但是无法引入），
 * * 无法用于 Option API 的 props
 */
export interface IGridProps<T> {
  /**
   * 列表的 meta，LowCode 需要。
   * * moduleId: number | string,
   * * idName: String,
   * * colOrder: Array<number|string>
   */
  gridMeta: IGridMeta,
  /**
   * table的列的属性， Object< IGridItem >
   * * id: number | string,
   * * colName: string, 字段名称
   * * label: string, 列的标签、标题
   * * width: number, 列的宽度
   * * align: EAlign, 内容对齐方式
   * * headerAlign: EAlign 列标题对齐方式
   */
  itemMeta: IGridItemList ,
  /**
   * 选择行的情况：IGridSelection
   * * dataId: '', 单选ID number 、string
   * * row: {}, 单选的数据对象 {}
   * * dataIds: [], 多选ID []
   * * rows: [] 多选的数据对象 []
   */
  selection: IGridSelection<T>, 
    
  /**
   * 绑定的数据 Array， 对应 data
   */
  dataList: Array<T>,
  /**
   * 是否显示表头，监听用
   */
  showHeader: boolean,
  /**
   * table的高度， Number
   */
  height: number,
  /**
   * 斑马纹，Boolean
   */
  stripe: boolean,
  /**
   * 纵向边框，Boolean
   */
  border: boolean,
  /**
   * 要高亮当前行，Boolean
   */
  highlightCurrentRow: boolean

}

 