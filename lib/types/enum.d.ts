/**
 * 控件类型的枚举
 */
export declare const enum EControlType {
    /**
     * 单行文本框
     */
    Text = 101,
    Area = 100,
    Password = 102,
    Email = 103,
    Tel = 104,
    Url = 105,
    Search = 106,
    Autocomplete = 107,
    Color = 108,
    Number = 110,
    Range = 111,
    Rate = 112,
    Date = 120,
    Week = 123,
    TimePicker = 130,
    TimeSelect = 132,
    File = 140,
    Picture = 141,
    Video = 142,
    Checkbox = 150,
    Switch = 151,
    Checkboxs = 152,
    Radios = 153,
    Select = 160,
    Selects = 161,
    SelectGroup = 162,
    SelectGroups = 160,
    SelectCascader = 164,
    SelectTree = 165,
    SelectTrees = 166
}
/**
 * 查询方式的枚举
 */
export declare const enum EFindKind {
    strEqual = 21,
    strNotEqual = 22,
    strInclude = 23,
    strNotInclude = 24,
    strStart = 25,
    strEnd = 26,
    strNotStart = 27,
    strNotEnd = 28,
    numEqual = 11,
    numNotEqual = 12,
    numInclude = 13,
    numNotInclude = 14,
    numStart = 15,
    numEnd = 16,
    numBetween = 17,
    numBetweenEnd = 18,
    numBetweenStart = 19,
    rangeInclude = 40,
    rangeIn = 41,
    rangeNotIn = 42
}
/**
 * 横向对齐方式，左、中、右
 */
export declare const enum EAlign {
    left = "left",
    center = "center",
    right = "right"
}
/**
 * 控件尺寸，大中小
 */
export declare const enum ESize {
    big = "large",
    def = "default",
    small = "small"
}
/**
 * 表单控件的分栏类型。
 * * card：卡片
 * * tab：多标签
 * * step：分步
 * * no：不分栏
 */
export declare const enum ESubType {
    card = "card",
    tab = "tab",
    step = "step",
    no = ""
}
