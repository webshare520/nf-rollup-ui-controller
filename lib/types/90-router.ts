
import type { AsyncComponentLoader, Component } from 'vue'

// 路由相关的类型

/**
 * 多层的菜单，转换为单层的菜单，便于用 key 查找 菜单
 */
export interface IMenuList {
  [key: string]: {
    /**
     * 菜单ID，类似路由的 name
     */
    menuId: string | number,
    /**
     * 浏览器的标题
     */
    title: string,
    /**
     * url 里的路径
     */
    path: string,
    /**
     * 路径集合 paths + '/' + _menu.path
     */
    paths: string,
    /**
     * 菜单对应的组件
     */
    component: Component
  }
}

/**
 * 一个菜单的类型
 */
export interface IMenu {
  /**
   * 菜单ID，类似路由的 name
   */
  menuId: string | number,
  /**
   * 浏览器的标题
   */
  title: string,
  /**
   * 导航ID
   */
  naviId?: string | number,
  /**
   * url 里的路径
   */
  path: string,
  /**
   * 图标，组件类型
   */
  icon: any,
  /**
   * 子菜单
   */
  childrens?: Array<IMenu>,
  /**
   * 打开的组件，树枝没有，树叶才有
   */
  component?: AsyncComponentLoader
}

/**
 * 创建路由-菜单时的对象类型
 */
export interface IMenus {
  /**
   * 基础路径，应对网站的二级目录
   */
  baseUrl: string,
  /**
   * 首页组件
   */
  home: any,
  /**
   * 菜单集合
   */
  menus: Array<IMenu>
}

/**
 * 当前激活的菜单的 key 和路径集合
 */
export interface ICurrentRoute {
  /**
   * 当前激活的菜单的ID
   */
  key: string,
  /**
   * 当前激活的多级菜单的路径集合
   */
  paths: Array<string | number>
}

/**
 * 泛型的约束
 */
export interface IProps {
  path: Array<string | number>,
  index: string | number
}

/**
 * 路由的类型
 */
export interface IRouter {
  /**
   * 基础路径，应对网站的二级目录
   */
  baseUrl: string,
  /**
   * 初始的浏览器标题
   */
  baseTitle: string,

  /**
   * 是否刷新进入
   */
  isRefresh: boolean,

  /**
   * 默认的首页
   */
  home: any,
  /**
   * 菜单集合
   */
  menus: Array<IMenu>,

  /**
   * 打开菜单的 tab 集合
   */
  tabs: Array<set>,

  /**
   * 当前激活的信息，菜单的ID和路径
   */
  currentRoute: ICurrentRoute
  
  // 内部方法
  /**
   * 初始化设置
   */
  setup: () => void,

  /**
   * 动态添加新路由
   */
  addRoute: (newMenus: IMenu, props = {}) => void,
  /**
   * 删除路由
   * @param { array } path 菜单的路径，[] 表示根菜单
   * @param { string | number } id 要删除的菜单ID
   */
  removeRoute: (path = [], id = '') => void,
 
  /**
   * 刷新时依据url加载组件
   */
  refresh: () => void

}
