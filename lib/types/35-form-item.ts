
/**
 * el-form-item 需要的 meta，也是 props
 * * colOrder: Array<number | string>,字段排序依据
 */
export interface IFormItemMeta {
  /**
   * 字段排序依据，也是显示依据。
   * 数组，v-for 的依据
   */
  colOrder: Array<number | string>

}