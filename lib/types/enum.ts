
/**
 * 控件类型的枚举
 */
export const enum EControlType {
  /**
   * 单行文本框
   */
  text = 101,
  textarea = 100,
  password = 102,
  email = 103,
  tel = 104,
  url = 105,
  search = 106,
  autocomplete = 107,
  color = 108,
  // 数字
  number = 110,
  range = 111,
  rate = 112,
  // 日期
  date = 120,
  datetime = 121,
  month = 122,
  week = 123,
  year = 124,
  daterange = 125,
  datetimerange = 126,
  monthrange = 127,
  dates = 128,
  
  // 时间
  timepicker = 130,
  timepickerrange = 131,
  timeselect = 132,
  timeselectrange = 133,
  // 上传
  file = 140,
  picture = 141,
  video = 142,
  // 选择等
  checkbox = 150,
  switch = 151,
  checkboxs = 152,
  radios = 153,
  // 下拉
  select = 160, // 单选下拉
  selects = 161, // 多选下拉
  selectGroup = 162, // 分组下拉单选
  selectGroups = 163, // 分组下拉多选
  selectCascader = 164, // 联动下拉
  selectTree = 165, // 树状下拉
  selectTrees = 166 // 树状多选
}
// export EControlType


/**
 * 查询方式的枚举
 */
export const enum EFindKind {
  // 字符串
  strEqual = 21, // '{col} = ?'
  strNotEqual = 22, // '{col} <> ?'
  strInclude = 23, // '{col} like "%?%"'
  strNotInclude = 24, // '{col} not like "%?%"'
  strStart = 25, // '{col} like "?%"'
  strEnd = 26, // '{col} like "%?"'
  strNotStart = 27, // '{col} not like "?%"'
  strNotEnd = 28, // '{col} not like "%?"'
  // 数字、日期时间
  numEqual = 11, // '{col} = ?'
  numNotEqual = 12, // '{col} <> ?'
  numInclude = 13, // '{col} > ?'
  numNotInclude = 14, // '{col} >= ?'
  numStart = 15, // '{col} < ?'
  numEnd = 16, // '{col} <= ?'
  numBetween = 17, // '{col} between ? and ?'
  numBetweenEnd = 18, // '{col} > ? and {col} <= ?'
  numBetweenStart = 19, // '{col} >= ? and {col} < ?'
  // 多选
  rangeInclude = 40, // '( {col} like %?% or {col} like %?% ... ) '
  rangeIn = 41, // '{col} in (?)'
  rangeNotIn = 42 // '{col} not in (?)'
  
}

/**
 * 横向对齐方式，left、center、right
 */
export const enum EAlign {
  left = 'left',
  center = 'center',
  right = 'right'
}

/**
 * 控件尺寸，大中小
 */
export const enum ESize {
  big = 'large',
  def = 'default',
  small = 'small'
}

/**
 * 表单控件的分栏类型。
 * * card：卡片
 * * tab：多标签
 * * step：分步
 * * no：不分栏
 */
export const enum ESubType {
  card = 'card',
  tab = 'tab',
  step = 'step',
  no = ''
}
