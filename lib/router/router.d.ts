import type { IMenuList, IMenus, IMenu, IRouter, ICurrentRoute, IProps } from '../types/90-router';
/**
 * 一个简单的路由
 * @param { string } baseUrl 基础路径
 * @param { components } home 基础路径
 * @param { array } menus 路由设置，数组，多级
 * * [{
 * * *  menuId: '菜单ID'
 * * *  naviId: '0', // 导航ID，可以不设置
 * * *  title: '标题',
 * * *  path: '路径',
 * * *  icon: Edit, // 图标组件
 * * *  component: () => import('./views/xxx.vue') // 要加载的组件，可以不设置
 * * *  childrens: [ // 子菜单，可以多级
 * * * *  {
 * * * * *  menuId: '菜单ID'
 * * * * *  title: '标题',
 * * * * *  path: '路径',
 * * * * *  icon: Edit, // 图标组件
 * * * * *  component: () => import('./views/xxx.vue') // 要加载的组件
 * * * * }
 * * * ]
 * * },
 * * 其他菜单
 * * ]
 */
declare class Router implements IRouter {
    baseUrl: string;
    baseTitle: string;
    isRefresh: boolean;
    home: any;
    menus: Array<IMenu>;
    menuList: IMenuList;
    tabs: any;
    currentRoute: ICurrentRoute;
    constructor(info: IMenus);
    /**
     * 初始化设置
     */
    setup: () => void;
    /**
     * 动态添加新路由
     */
    addRoute<T extends IProps>(newMenus: Array<IMenu>, props: T): void;
    /**
     * 删除路由
     * @param { array } path 菜单的路径，[] 表示根菜单
     * @param { string | number } id 要删除的菜单ID
     */
    removeRoute(path?: never[], id?: string): void;
    /**
     * 刷新时依据url加载组件
     */
    refresh: () => void;
    /**
     * 加载路由指定的组件
     * @returns
     */
    getComponent: () => any;
    /**
     * 删除tab
     * @param { string } key
     * @returns
     */
    removeTab: (key: any) => void;
    /**
     * 安装插件
     * @param {*} app
     */
    install: (app: any) => void;
}
/**
 * 创建简易路由
 */
declare const createRouter: (info: IMenus) => Router;
/**
 * 获取路由
 * @returns
 */
declare function useRouter(): Router;
export { createRouter, useRouter };
