
import type {
  IGridMeta,
  IGridTable,
  IGridItem,
  IGridSelection
} from '../../types/50-grid'

import type {
  IDragInfo,
  IDragEvent
} from '../../types/70-drag'

/**
 * 设置 dom 拖拽事件
 * @param itemMeta 可以拖拽的 dom
 * @param colOrder dom 的 innerText 和字段ID的对应关系
 * @param deleteDom 拖拽信息
 * @param table 拖拽事件
 */
export default function setDrag (
  itemMeta: IGridItem,
  colOrder: Array<number|string>,
  deleteDom: () => void,
  table: HTMLTableElement
) {
    /**
   * 移除选择的字段
   * @param { IDragInfo } dragInfo 拖拽信息
   */
  const _setRemove = (dragInfo: IDragInfo) => {
    const col = itemMeta[dragInfo.sourceId]
    // 调用外部的确认对话框
    deleteDom(col, () => {
      // 确认移除，才会执行回调
      colOrder.splice(dragInfo.sourceIndex, 1)
    }) 
  }

  /**
   * 设置th的对齐方式
   */
  const _setThAlgin = (dragInfo: IDragInfo) => {
    // 判断 th 还是 td
    const alignKind = (dragInfo.ctrl) ? 'header-align' : 'align'
    const col = itemMeta[dragInfo.sourceId]
    // 判断：左中右
    switch (col[alignKind]) {
      case 'left':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'center'
        }
        break
      case 'center':
        if (dragInfo.isLeft) {
          col[alignKind] = 'left'
        } else {
          col[alignKind] = 'right'
        }
        break
      case 'right':
        if (dragInfo.isLeft) {
          col[alignKind] = 'center'
        } else {
          col[alignKind] = 'right'
        }
        break
    }
  }

  // 调整 th 的宽度后，记录新的宽度
  const _setThWidth = (e) => {
    // 等待刷新
    setTimeout(() => {
      // 监听事件，获取调整后的 th 的宽度
      // for (let i = 1; i < tdCount; i++) {
      const arr = Array.from(table.rows[0].cells)
      let i = -1
      arr.forEach(element => {
        if (i === -1) { // 跳过第一列
          i = 0
        } else {
          itemMeta[colOrder[i++]].width = element.offsetWidth
        }
      })
    }, 1000)
  }

  return {

  }

}
   