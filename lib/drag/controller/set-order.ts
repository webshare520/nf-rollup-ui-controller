
import type {
  IGridMeta,
  IGridTable,
  IGridItem,
  IGridSelection
} from '../../types/50-grid'


import type {
  IDragInfo,
  IDragEvent
} from '../types/70-drag'

/**
 * 设置排序，支持 table 的 th，form 的 div，find 的 div
 * * 调整顺序
 * * 交换位置
 * @param colOrder 用于排序
 * @param dragInfo 拖拽信息
 */
export default function setOrder (colOrder: Array<number|string>, dragInfo: IDragInfo) {
  /**
   * 交换两个th的位置
   */
  const _swapPlaces = () => {
    // 交换
    colOrder[dragInfo.sourceIndex] = dragInfo.targetId
    colOrder[dragInfo.targetIndex] = dragInfo.sourceId
  }

  /**
   * 插入 th 后调整顺序
   */
  const _order = () => {
    // 判断前插、后插。后插：偏移 0；前插：偏移 1
    const offsetTarget = dragInfo.isLeft ? 0 : 1
    // 判断前后顺序。》 1；《 0
    const offsetSource = dragInfo.sourceIndex < dragInfo.targetIndex ? 0 : 1

    // 插入源
    colOrder.splice(dragInfo.targetIndex + offsetTarget, 0, dragInfo.sourceId)
    // 删除源
    colOrder.splice(dragInfo.sourceIndex + offsetSource, 1)
  }

  // 源和目标不同，排序
  if (dragInfo.ctrl) {
    // 交换
    _swapPlaces()
  } else {
    // 插入
    _order()
  }
}
 
