import type { IDragInfo } from '../types/70-drag';
/**
 * 设置排序，支持 table 的 th，form 的 div，find 的 div
 * * 调整顺序
 * * 交换位置
 * @param colOrder 用于排序
 * @param dragInfo 拖拽信息
 */
export default function setOrder(colOrder: Array<number | string>, dragInfo: IDragInfo): void;
