import type { IGridMeta, IGridItemList } from '../types/50-grid';
import type { IDragInfo } from '../types/70-drag';
/**
 * 数据列表的拖拽相关的操作，记录调整后的td 的宽度，调整td的顺序，移除td
 * @param gridMeta 用于排序 colOrder
 * @param itemMeta 获取列的信息
 * @param table table 的 dom
 * @param deleteDom 移除 dom 的确认对话框
 * @param modCol ctrl + 单击 th 的事件
 * @returns
 */
export default function gridDrag(gridMeta: IGridMeta, itemMeta: IGridItemList, table: HTMLTableElement, deleteDom: (col: any, cb: () => void) => void, modCol: (colId: string | number, dragInfo: IDragInfo, event: any) => void): {
    girdSetup: () => void;
};
