import type { IFormItemList } from '../types/20-form-item';
import type { IFromMeta } from '../types/30-form';
import type { IDragInfo } from '../types/70-drag';
/**
 *  数据列表的拖拽相关的操作，记录调整后的td 的宽度，调整td的顺序，移除td
 * @param formMeta 列表的 meta
 * @param itemMeta 表单子控件 集合
 * @param form 表单子控件 集合
 * @param deleteCol 移除 th 的确认对话框
 * @returns
 */
export default function formDrag(formMeta: IFromMeta, itemMeta: IFormItemList, form: HTMLTableElement, deleteCol: (col: any, cb: () => void) => void, modCol: (colId: string | number, dragInfo: IDragInfo, event: any) => void): {
    formSetup: () => void;
};
