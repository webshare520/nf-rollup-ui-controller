declare const _default: {
    /**
     * axios 访问的时候使用的基础路径
     */
    baseUrl: string;
    /**
     * 传入 axios 对象，或者实例
     */
    axios: any;
};
/**
 * 设置配置信息，便于内部访问后端API
 */
export default _default;
