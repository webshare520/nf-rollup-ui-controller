import type { PropType } from 'vue';
import type { IGridMeta, IGridItem, IGridSelection } from '../types/50-grid';
/**
 * 表单控件需要的属性propsForm
 */
export declare const gridProps: {
    gridMeta: {
        type: PropType<IGridMeta>;
    };
    /**
     * 高度
     */
    height: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
    /**
     * 斑马纹
     */
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 纵向边框
     */
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 列的宽度是否自撑开
     */
    fit: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
    * 要高亮当前行，Boolean
    */
    highlightCurrentRow: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * table的列的 meta
     */
    itemMeta: {
        type: PropType<{
            [key: string]: IGridItem;
            [key: number]: IGridItem;
        }>;
    };
    /**
     * 选择的情况 IGridSelection
     */
    selection: {
        type: PropType<IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    /**
     * 绑定的数据
     */
    dataList: {
        type: PropType<any[]>;
        default: () => never[];
    };
};
