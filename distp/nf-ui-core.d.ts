
import type { PropType } from 'vue';
import type { 
    IFromMeta 
} from './types/30-form';

import type { IFormItemList } from './types/20-form-item';

/**
 * baseUrl: string，axios 访问的时候使用的基础路径
 * * axios: any，传入 axios 对象，或者实例
 */
export declare const serviceConfig: {
    /**
     * axios 访问的时候使用的基础路径
     */
    baseUrl: string;
    /**
     * 传入 axios 对象，或者实例
     */
    axios: any;
};

/**
 * 访问后端API
 * @param url 路径
 * @param query url后面的查询条件
 * @param body body 对象
 */
export function service(url: any, query?: string, body?: null): Promise<unknown>;

/**
 * 查询控件需要的属性 findProps
 */
declare const findProps: {
    moduleId: (StringConstructor | NumberConstructor)[];
    active: ObjectConstructor;
    colOrder: {
        type: PropType<(string | number)[]>;
        default: () => never[];
    };
    findValue: {
        type: ObjectConstructor;
        default: () => void;
    };
    findArray: {
        type: ObjectConstructor;
        default: () => never[];
    };
    quickFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    allFind: {
        type: ArrayConstructor;
        default: () => never[];
    };
    customer: {
        type: ArrayConstructor;
        default: () => void;
    };
    customerDefault: {
        type: ArrayConstructor;
        default: number;
    };
    reload: {
        type: BooleanConstructor;
        default: boolean;
    };
    itemMeta: {
        type: PropType<IFromMeta>;
        default: () => void;
    };
};

/**
 * 表单控件需要的属性
 */
 export declare const formProps: {
    /**
     * 表单的完整的 model
     */
    model: {
        type: PropType<any>;
        required: boolean;
    };
    /**
     * 根据选项过滤后的 model
     */
    partModel: {
        type: PropType<any>;
        default: () => {};
    };
    /**
     * 表单控件的 meta
     */
    formMeta: {
        type: PropType<IFromMeta>;
        default: () => {};
    };
    /**
     * 表单控件的子控件的 meta 集合
     */
    itemMeta: {
        type: PropType<IFormItemList>;
        default: () => {};
    };
    /**
     * 标签的后缀
     */
    labelSuffix: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 标签的宽度
     */
    labelWidth: {
        type: StringConstructor;
        default: string;
    };
    /**
     * 控件的规格
     * * 【element-plus】large / default / small 三选一
     */
    size: {
        type: StringConstructor;
        default: string;
    };
};

/**
 * 表单控件的管理类
 * * 创建 v-model，创建局部 model，设置行列、排序相关的处理
 * @param formMeta 表单控件的 meta
 * @param model 表单的 model
 * @param partModel 表单的部分 model
 * @returns
 */
 export default function formController(formMeta: IFromMeta, itemMeta: IFormItemList, model: any, partModel?: any): {
    formColSpan: {
        [x: number]: number;
    };
    showCol: {
        [x: string]: boolean;
        [x: number]: boolean;
    };
    setFormColShow: () => void;
};

/**
 * @function 创建表单的 v-model
 * @description 依据表单的 meta，自动创建 model
 * @param meta 表单子控件的meta
 * @param colOrder 需要哪些字段，以及顺序
 */
 export declare const createModel: (meta: IFormItemList, colOrder: Array<number | string>) => {
    [x: string]: any;
    [x: number]: any;
};


import type { IGridMeta, IGridItem, IGridSelection } from './types/50-grid';
/**
 * 表单控件需要的属性propsForm
 */
 export declare const gridProps: {
    gridMeta: {
        type: PropType<IGridMeta>;
    };
    /**
     * 高度
     */
    height: {
        type: (StringConstructor | NumberConstructor)[];
        default: number;
    };
    /**
     * 斑马纹
     */
    stripe: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 纵向边框
     */
    border: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 列的宽度是否自撑开
     */
    fit: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
    * 要高亮当前行，Boolean
    */
    highlightCurrentRow: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * table的列的 meta
     */
    itemMeta: {
        type: PropType<{
            [key: string]: IGridItem;
            [key: number]: IGridItem;
        }>;
    };
    /**
     * 选择的情况 IGridSelection
     */
    selection: {
        type: PropType<IGridSelection>;
        default: () => {
            dataId: string;
            row: {};
            dataIds: never[];
            rows: never[];
        };
    };
    /**
     * 绑定的数据
     */
    dataList: {
        type: PropType<any[]>;
        default: () => never[];
    };
};


import type { IMenuList, IMenus, IMenu, IRouter, ICurrentRoute, IProps 
} from './types/90-router';
/**
 * 一个简单的路由
 * @param { string } baseUrl 基础路径
 * @param { components } home 基础路径
 * @param { array } menus 路由设置，数组，多级
 * * [{
 * * *  menuId: '菜单ID'
 * * *  naviId: '0', // 导航ID，可以不设置
 * * *  title: '标题',
 * * *  path: '路径',
 * * *  icon: Edit, // 图标组件
 * * *  component: () => import('./views/xxx.vue') // 要加载的组件，可以不设置
 * * *  childrens: [ // 子菜单，可以多级
 * * * *  {
 * * * * *  menuId: '菜单ID'
 * * * * *  title: '标题',
 * * * * *  path: '路径',
 * * * * *  icon: Edit, // 图标组件
 * * * * *  component: () => import('./views/xxx.vue') // 要加载的组件
 * * * * }
 * * * ]
 * * },
 * * 其他菜单
 * * ]
 */
declare class Router implements IRouter {
    baseUrl: string;
    baseTitle: string;
    isRefresh: boolean;
    home: any;
    menus: Array<IMenu>;
    menuList: IMenuList;
    tabs: any;
    currentRoute: ICurrentRoute;
    constructor(info: IMenus);
    /**
     * 初始化设置
     */
    setup: () => void;
    /**
     * 动态添加新路由
     */
    addRoute<T extends IProps>(newMenus: Array<IMenu>, props: T): void;
    /**
     * 删除路由
     * @param { array } path 菜单的路径，[] 表示根菜单
     * @param { string | number } id 要删除的菜单ID
     */
    removeRoute(path?: never[], id?: string): void;
    /**
     * 刷新时依据url加载组件
     */
    refresh: () => void;
    /**
     * 加载路由指定的组件
     * @returns
     */
    getComponent: () => any;
    /**
     * 删除tab
     * @param { string } key
     * @returns
     */
    removeTab: (key: any) => void;
    /**
     * 安装插件
     * @param {*} app
     */
    install: (app: any) => void;
}
/**
 * 创建简易路由
 */
declare const createRouter: (info: IMenus) => Router;
/**
 * 获取路由
 * @returns
 */
declare function useRouter(): Router;
export { createRouter, useRouter };
import type { IOptionList, IOptionTree, IFormItemProps } from './types/20-form-item';
/**
 * 基础控件的共用属性，即表单子控件的基础属性
 */
export declare const itemProps: {
    /**
     * * columnId：number | string，字段ID、控件ID
     * * colName：string，字段名称，控件使用 model 的哪个属性，多个字段名称用 “_” 分割
     * * controlType：number，EControlType，控件类型，表单控件据此加载对应的子控件
     * * defValue: any，子控件的默认值
     * * optionList：IOptionList | IOptionTree，控件的备选项，单选、多选、等控件需要
     * * webAPI：IWebAPI，访问后端API的参数
     * * delay：Number，防抖的时间间隔，0：不用防抖。
     * * events：事件集合，用于防抖，IEventDebounce
     * *
     */
    formItemMeta: {
        type: PropType<IFormItemProps>;
        default: () => {};
    };
    optionList: {
        type: PropType<(IOptionList | IOptionTree)[]>;
        default: () => never[];
    };
    /**
     * // 表单的 model，可以整体传入，便于子控件维护字段值。
     */
    model: {
        type: ObjectConstructor;
    };
    /**
     * 是否显示可清空的按钮，默认显示
     */
    clearable: {
        type: BooleanConstructor;
        default: boolean;
    };
    /**
     * 浮动的提示信息，部分控件支持
     */
    title: {
        type: StringConstructor;
        default: string;
    };
};

import type { IFormItemMeta } from './types/20-form-item';
interface IProps1 {
    model: any;
    formItemMeta: IFormItemMeta;
    modelValue: any;
}
/**
 * 表单子控件的控制函数
 * @param props 组件的 props
 * @param emit 组件的 emit
 * @returns
 */
export default function itemController<T extends IProps1, U>(props: T, emit: (event: U, ...args: any[]) => void, events?: any): {
    value: any;
    run: () => void;
    clear: (e: any) => void;
};